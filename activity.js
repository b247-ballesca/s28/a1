// 1
db.users.insert({
	name: "Single",
	accomodates: 2,
	price: 1000,
        description: "A simple room with all the basic necessities",
        roomsAvalable: 10,
	isAvailable: false
},); 

// 2
db.users.insert({
	name: "double",
	accomodates: 3,
	price: 2000,
        description: "A room fit for a small family going on a vacation",
        roomsAvalable: 5,
	isAvailable: false
},{
	name: "Queen",
	accomodates: 4,
	price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway",
        roomsAvalable: 15,
	isAvailable: false
}); 
// 3
db.users.find({ name: "double" });

// 4

db.users.updateOne(
	{ firstName: "Queen"},
	{
		$set : {
			roomsAvalable: 0,
		}
	}
);

// 5
db.users.deleteOne({
		roomsAvalable: 0,
});


